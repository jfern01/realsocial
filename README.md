realsocial
==========

[View Demo](http://www.josefernandez.me/plugin-real-social/)

Social Icons Plugin for WordPress (Shortcode + Widget)

Shortcode usage:

	[real_social size="50" linkedin="https://www.linkedin.com/pub/jose-fernandez/40/795/891"]

Now using [simple-icons](https://github.com/danleech/simple-icons)

