<?php
/*
Plugin Name: Real Social
Plugin URI: josefernandez.me/plugin-real-social/
Description: Social icons plugin, done right.
Version: 0.2
Author: Jose Fernandez
Author URI: http://josefernandez.me
License: GPLv2
*/

wp_enqueue_style( 'realsocial', plugins_url('realsocial.css', __FILE__));

function real_social_shortcode($atts, $content) {
	$output = '';
	$settings = array(
		'size' => 50, //default size
		'max_size' => 128);

	$colorSettings = json_decode(file_get_contents(plugins_url('assets/simple-icons.json', __FILE__)), true);

	foreach ($atts as $network => $networkURL)
	{
		if ($network == 'size')
		{
			if (is_numeric($networkURL))
			{
				if ($networkURL >= $settings['max_size'])
				{
					$settings['size'] = $settings['max_size'];
				}
				else
				{
					$settings['size'] = $networkURL;
				}
			}
		}
		else
		{
			$color = '';
			foreach ($colorSettings as $item)
			{
				if ($item['name'] == $network)
				{
					$color .= $item['hex'];
				}
			}
			$output .= '<a style="background-color: #'.$color.';" class="network-url" href="'.$networkURL.'"><img class="network-image" src="'.plugins_url('assets/'.$network.'/'.$network, __FILE__).'-128.png" width="'.$settings['size'].'px"/></a>';
		}
	}
	//return $debug;
	return $output;
}

add_shortcode('real_social', 'real_social_shortcode');



//WIDGET TIME

class realsocial_widget extends WP_Widget
{
	var $networkList;
	var $settings;

	function realsocial_widget()
	{
		$this->networkList = json_decode(file_get_contents(plugins_url('assets/simple-icons.json', __FILE__)), true);
		$this->settings = array( 'size' => 50, 'max_size' => 128);
		parent::WP_Widget(false, $name = 'RealSocial');
	}

    function widget($args, $instance)
    {
		foreach ($instance as $network => $networkURL)
		{
			if ($network == 'size')
			{
				if (is_numeric($networkURL))
				{
					if ($networkURL >= $this->settings['max_size'])
					{
						$this->settings['size'] = $this->settings['max_size'];
					}
					else
					{
						$this->settings['size'] = $networkURL;
					}
				}
			}
			elseif ($networkURL != '')
			{
				$color = '';

				foreach ($this->networkList as $item)
				{
					if ($item['name'] == $network)
					{
						$color = $item['hex'];
					}
				}

				echo '<a style="background-color: #'.$color.';" class="network-url" href="'.$networkURL.'" target=""><img class="network-image" src="'.plugins_url('assets/'.$network.'/'.$network, __FILE__).'-128.png" width="'.$this->settings['size'].'px"/></a>';
			}
		}
	}

    function update($new_instance, $old_instance)
    {
		$instance = $old_instance;

		$instance['size'] = strip_tags($new_instance['size']);

		foreach ($instance as $control => $value)
		{
			$instance[$control] = strip_tags($new_instance[$control]);
		}

	    return $new_instance;
    }

    function form($instance)
    { ?>
    	<p>
			<label for="<?php echo $this->get_field_id('size'); ?>"><?php _e('Size', 'wp_widget_plugin'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('size'); ?>" name="<?php echo $this->get_field_name('size'); ?>" type="text" value="<?php echo esc_attr($instance['size']); ?>" />
		</p>

		<?php foreach($this->networkList as $item) : ?>
		<p>
			<label for="<?php echo $this->get_field_id($item['name']); ?>"><?php _e(ucfirst($item['name']), 'wp_widget_plugin'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id($item['name']); ?>" name="<?php echo $this->get_field_name($item['name']); ?>" type="text" value="<?php echo esc_attr($instance[$item['name']]); ?>" />
		</p>
		<?php
		endforeach;
    }
}

add_action('widgets_init', create_function('', 'return register_widget("realsocial_widget");'));



?>